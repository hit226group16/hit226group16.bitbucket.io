$(document).ready(function(){	
    $("#myForm").submit(function (event){
		if(!validateEmail($("#email").val())){
			$("#emailError").html("Please enter a valid email address.");
			event.preventDefault();
			$("#email").focus();
			$("#emailError").addClass("error");
		}else{
			$("#emailError").removeClass("error");
		}
		if(!validateTnum($("#Tnumber").val())){
			$("#TnumberError").html("Please type your telephone number.");
			event.preventDefault();
			$("#Tnumber").focus();
			$("#TnumberError").addClass("error");
		}else{
			$("#TnumberError").removeClass("error");
		}
		if($("#Tnumber").val().startsWith("+614")){
			$("#Tnumber").val().replace("+614", "0");
		}
		if(!validateDtg($("#dtg").val())){
			$("#dtgError").html("Please pick a date.");
			event.preventDefault();
			$("#dtg").focus();
			$("#dtgError").addClass("error");
		}else{
			$("#dtgError").removeClass("error");
			
		}
	});
	$("#email").keyup(function (){
		if(validateEmail($("#email").val())){
			$("#emailError").removeClass("error");
			$("#emailError").html("");
		}
	});
	$("#Tnumber").keyup(function (){
		if(validateTnum($("#Tnumber").val())){
			$("#TnumberError").removeClass("error")
			$("#TnumberError").html("");
		}
	});
	$("#dtg").mouseleave(function (){
		if(validateDtg($("#dtg").val())){
			$("#dtgError").removeClass("error")
			$("#dtgError").html("");
		}
	});
	$("#dtg").datepicker();
	
function validateEmail(email) {
	var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
return re.test(email);}
function validateTnum(Tnumber){
	var mpa = /^\d{10}$/;
return mpa.test(Tnumber);}
function validateDtg(dtg){
	var dt = /(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d/;
return dt.test(dtg);}
	
});
